package servico;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Hibernate;

import entidades.Autor;

@Path("/autores")
public class AutorResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Autor> getAutores(){
		
		return new ArrayList<Autor>();
		
	}
	
}
