package servico.util;

import java.lang.reflect.Type;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.ws.rs.ext.Provider;

import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.spi.inject.Injectable;
import com.sun.jersey.spi.inject.InjectableProvider;

@Provider
public class EJBProvider implements InjectableProvider<EJB, Type> {

	 public ComponentScope getScope() {
         return ComponentScope.Singleton;
}

public Injectable<Object> getInjectable(ComponentContext cc, EJB ejb, Type t) {
         if (!(t instanceof Class)) return null;

         try {            
             Context ic = new InitialContext();

             final Object o = ic.lookup("java:global/BK_EAR/BK_EJB/"+ejb.name());

             return new Injectable<Object>() {
                   public Object getValue() {
                       return o;
                   }
             };
         } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
 }
	
}
