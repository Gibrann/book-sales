package servico.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import entidades.Livro;

@XmlRootElement
public class CategoriaVO {

	private Integer id;
	private String nome;
	private List<Livro> livros;
	private Integer quatidade;
	
	
	public Integer getQuatidade() {
		return quatidade;
	}
	
	public List<Livro> getLivros() {
		return livros;
	}

	public void setLivros(List<Livro> livros) {
		this.livros = livros;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
