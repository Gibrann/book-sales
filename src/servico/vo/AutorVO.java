package servico.vo;

import java.util.Set;

import entidades.Livro;

public class AutorVO {

	private Integer id;
	private String nome;
	private Set<Livro> livros;
	
	
	public Set<Livro> getLivros() {
		return livros;
	}
	public void setLivros(Set<Livro> livros) {
		this.livros = livros;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
