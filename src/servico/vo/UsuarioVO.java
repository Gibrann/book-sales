package servico.vo;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import entidades.Conta;
import entidades.Livro;

@XmlRootElement
public class UsuarioVO {

	private Integer id;
	private String nome;
	private String senha;
	private String email;
	private Conta conta;
	private Set<Livro> livros;
	
	public UsuarioVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Set<Livro> getLivros() {
		return livros;
	}
	public void setLivros(Set<Livro> livros) {
		this.livros = livros;
	}
	
}
