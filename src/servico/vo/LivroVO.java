package servico.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import entidades.Autor;
import entidades.Categoria;
import entidades.Usuario;

@XmlRootElement
public class LivroVO {

	private Integer id;
	private String titulo;
	private String edicao;
	private Integer ano;
	private String resumo;
	private Categoria categoria;
	private Double preco;
	private byte [] foto;
	private Usuario usuario;
	private List<Autor> autores;
	
	
	public LivroVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEdicao() {
		return edicao;
	}
	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public byte [] getFoto() {
		return foto;
	}
	public void setFoto(byte [] bs) {
		this.foto = bs;
	}
	public List<Autor> getAutores() {
		return autores;
	}
	public void setAutores(List<Autor> autores) {
		this.autores = autores;
	}
	
	
}
