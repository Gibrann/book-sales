package servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import servico.vo.CategoriaVO;
import br.unifor.bean.CategoriaBeanRemote;
import entidades.Categoria;

@Path("/categorias")
public class CategoriaResource {

	@EJB(name="CategoriaBean")
	private CategoriaBeanRemote beanRemote;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CategoriaVO> getCategorias(){
		
		List<Categoria> categorias = beanRemote.listar();
		
		List<CategoriaVO> categoriasVO = new ArrayList<CategoriaVO>(); 
		
		for (Categoria categoria : categorias) {
			CategoriaVO c = new CategoriaVO();
			c.setId(categoria.getId());
			c.setNome(categoria.getNome());
			categoriasVO.add(c);
		}
		
		
		return categoriasVO;
		
	}
	
}
