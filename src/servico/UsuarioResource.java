package servico;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import servico.vo.UsuarioVO;
import br.unifor.bean.UsuarioBeanRemote;
import entidades.Usuario;

@Path("/usuario")
public class UsuarioResource {

	@EJB(name="UsuarioBean")
	private UsuarioBeanRemote usuarioBean;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UsuarioVO validarUsuario(UsuarioVO usuario){
		if(autenticarUsuario(usuario)){
			return usuario;
		}else{
			return null;
		}
	}
	
	private boolean autenticarUsuario(UsuarioVO usuario){
		List<Usuario> usuarios = usuarioBean.listar();
		for (int i = 0; i < usuarios.size(); i++) {
			if(usuarios.get(i).getEmail().equalsIgnoreCase(usuario.getEmail()) 
					&& usuarios.get(i).getSenha().equals(usuario.getSenha())){
					usuario.setNome(usuarios.get(i).getNome());
					usuario.setId(usuarios.get(i).getId());
					return true;
			}
		}
		
		return false;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String atualizar(UsuarioVO usuario){
		
		Usuario user = new Usuario();
		
		user.setEmail(usuario.getEmail());
		user.setId(usuario.getId());
		user.setNome(usuario.getNome());
		user.setSenha(usuario.getSenha());
		
		usuarioBean.atualizar(user);
		return "Usuário "+usuario.getNome()+" atualizado com sucesso";
	}
	
	@POST
	@Path("/adicionar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String adicionar(UsuarioVO usuario){
		
		Usuario user = new Usuario();
		user.setEmail(usuario.getEmail());
		user.setNome(usuario.getNome());
		user.setSenha(usuario.getSenha());
		
		usuarioBean.inserir(user);
		
		return "Usuário "+usuario.getNome()+" cadastrado com sucesso";
	}
	
	@GET
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UsuarioVO getById(@PathParam("id")int id){
		
		Usuario user = usuarioBean.getById(id);
		
		UsuarioVO vo = new UsuarioVO();
		
		vo.setEmail(user.getEmail());
		vo.setNome(user.getNome());
		vo.setId(user.getId());
		
		return vo;
	}
	
	
	
}
