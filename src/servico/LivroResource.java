package servico;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import servico.vo.LivroVO;
import br.unifor.bean.LivroBeanRemote;
import entidades.Livro;

@Path("/livros")
public class LivroResource {

	@EJB(name="LivroBean")
	private LivroBeanRemote beanRemote;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<LivroVO> getLivros(){
		
		List<Livro> listLivros = beanRemote.lista();
		
		List<LivroVO> listLivrosVO = new ArrayList<LivroVO>();
		
		for (Livro livro : listLivros) {
			LivroVO book = new LivroVO();
			book.setId(livro.getId());
			book.setAno(livro.getAno());
			book.setAutores(livro.getAutores());
			book.setCategoria(livro.getCategoria());
			book.setEdicao(livro.getEdicao());
			book.setFoto(livro.getFoto());
			book.setTitulo(livro.getTitulo());
			book.setPreco(livro.getPreco());
			book.setResumo(livro.getResumo());
			book.setUsuario(livro.getUsuario());
			listLivrosVO.add(book);
		}
		
		return listLivrosVO;
		
	}
	
	@GET
	@Path("/user/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<LivroVO> getLivrosByUser(@PathParam("id") int id){
		
		List<Livro> listLivros = beanRemote.listaByUser(id);
		
		List<LivroVO> listLivrosVO = new ArrayList<LivroVO>();
		
		for (Livro livro : listLivros) {
			LivroVO book = new LivroVO();
			book.setId(livro.getId());
			book.setAno(livro.getAno());
			book.setAutores(livro.getAutores());
			book.setCategoria(livro.getCategoria());
			book.setEdicao(livro.getEdicao());
			book.setFoto(livro.getFoto());
			book.setTitulo(livro.getTitulo());
			book.setPreco(livro.getPreco());
			book.setResumo(livro.getResumo());
			book.setUsuario(livro.getUsuario());
			listLivrosVO.add(book);
		}
		
		return listLivrosVO;
		
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public LivroVO getById(@PathParam("id") int id){
		
		Livro livro = beanRemote.getById(id);
		
		LivroVO book = new LivroVO();
		book.setId(livro.getId());
		book.setAno(livro.getAno());
		book.setAutores(livro.getAutores());
		book.setCategoria(livro.getCategoria());
		book.setEdicao(livro.getEdicao());
		book.setFoto(livro.getFoto());
		book.setTitulo(livro.getTitulo());
		book.setPreco(livro.getPreco());
		book.setResumo(livro.getResumo());
		book.setUsuario(livro.getUsuario());
		
		
		return book; 
	}
	
	@GET
	@Path("busca/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<LivroVO> getByTitle(@PathParam("id") String filtro){
		List<LivroVO> listLivros = new ArrayList<LivroVO>();
		
		List<LivroVO> livros = getLivros();
		
		for (int i=0;i<livros.size();i++) {
			LivroVO livro = livros.get(i);
			boolean res = livro.getTitulo().contains(filtro);
			if(res){
				listLivros.add(livros.get(i));
			}
		}
		
		return listLivros;
	}
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String remover(@PathParam("id") Integer id){
		
		beanRemote.remover(id);
		
		return "Livro removido com sucesso";
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String adicionar(LivroVO livro){
		
		Livro book = new Livro();
		
		book.setAno(livro.getAno());
		book.setAutores(livro.getAutores());
		book.setCategoria(livro.getCategoria());
		book.setEdicao(livro.getEdicao());
		book.setFoto(livro.getFoto());
		book.setPreco(livro.getPreco());
		book.setResumo(livro.getResumo());
		book.setTitulo(livro.getTitulo());
		book.setUsuario(livro.getUsuario());
		
		beanRemote.inserir(book);
		return "salvo";
	}
	
	@POST
	@Path("foto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String adicionarFoto(LivroVO livro){
		
		Livro book = new Livro();
		
		book.setFoto(livro.getFoto());
		
		beanRemote.inserirFoto(book);
		return "salvo";
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String atualizar(LivroVO livro){
		
		Livro book = new Livro();
		
		book.setAno(livro.getAno());
		book.setId(livro.getId());
		book.setAutores(livro.getAutores());
		book.setCategoria(livro.getCategoria());
		book.setEdicao(livro.getEdicao());
		book.setFoto(livro.getFoto());
		book.setPreco(livro.getPreco());
		book.setResumo(livro.getResumo());
		book.setTitulo(livro.getTitulo());
		book.setUsuario(livro.getUsuario());
		
		beanRemote.atualizar(book);
		return "salvo";
	}
	
	
}
