(function(){
	var bk = angular.module('bookSales',['ngRoute', 'controllersModules','Services','flow','ngDialog']);
	
	bk.config(function(flowFactoryProvider, ngDialogProvider){
		flowFactoryProvider.defaults = {
				permanentErrors: [404, 500, 501],
			    maxChunkRetries: 1,
			    chunkRetryInterval: 5000,
			    simultaneousUploads: 4,
			    singleFile: true
		 };
		
	});
	
	bk.run(function($rootScope,ngDialog,$location){
		$rootScope.messages = {};
		if(window.localStorage.getItem('user')){
			var user = JSON.parse(window.localStorage.getItem('user'));
			$rootScope.userEmail = user.email;
		}
		
		$rootScope.openPaymentModal = function(){
			if(!$rootScope.isLogado || $rootScope.isLogado == undefined){
				$location.url("login");
				$rootScope.messages.warnMessage ="Você precisa estar logado para acessar esta página! =(";
				return;
			}else{
				 ngDialog.open({
		             template: "templates/pagamento-livro.html"
		         });
			}
			
			$rootScope.openSecond = function () {
				ngDialog.close();
				ngDialog.open({
					template: '<h3><a href="" ng-click="closeSecond()">Obrigado pela compra!</a></h3>',
					plain: true,
					closeByEscape: false
				});
				$location.url("livros");
			}
			
		}
		
	});
	
	bk.config(['$routeProvider',
	           function($routeProvider, $locationProvider) {
		
		$routeProvider.
		when('/login', {
			templateUrl: 'templates/login.html',
			controller: 'UsuarioController'
		}).
		when('/livros', {
			templateUrl: 'templates/principal.html',
			controller: 'LivroController'
		}).
		when('/detalhe_livro/:livroId', {
			templateUrl: 'templates/detalhe-livro.html',
			controller: 'LivroController'
		}).
		when('/cadastro_usuario', {
			templateUrl: 'templates/cadastro-usuario.html',
		    controller: 'UsuarioController'	
		}).
		when('/categoria/:categoria', {
			templateUrl: 'templates/principal.html',
			controller: 'LivroController'
		}).
		when('/pesquisa/:filtro', {
			templateUrl: 'templates/pesquisa.html',
			controller: 'PesquisaController'
		}).
		when('/perfil', {
			templateUrl: 'templates/perfil.html',
			controller: 'PerfilController'
		}).
		when('/meus-livros', {
			templateUrl: 'templates/meus-livros.html',
			controller: 'MeusLivrosController'
		}).
		when('/novo-livro', {
			templateUrl: 'templates/cadastrar-livro.html',
			controller: 'CadastroLivroController'
		}).
			when('/atualizar-livro/:id', {
			templateUrl: 'templates/cadastrar-livro.html',
			controller: 'CadastroLivroController'
		})
		.otherwise({redirectTo: '/meus-livros'});
	}]);

})();
