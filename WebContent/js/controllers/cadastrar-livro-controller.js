facController.controller('CadastroLivroController',function($scope,BSRequest,$rootScope,$location,$routeParams,$http){
	
	if(!$rootScope.isLogado || $rootScope.isLogado == undefined){
		$location.url("login");
		$rootScope.messages.warnMessage ="Você precisa estar logado para acessar esta página! =(";
		return;
	}
	
	$scope.editar = false; 
	
	
	$scope.livro = {};
	$scope.livro.categoria = $rootScope.categorias[0];
	$scope.livroFoto = {}
	$scope.file = {}
	$rootScope.messages = {}
	$scope.livro.autores = []
	$scope.obj = {};
	$scope.addAutor = function(){
		var autor = {"nome": $scope.livro.autor}
		if($scope.livro.autores == undefined){
			$scope.livro.autores = []
		}
		$scope.livro.autores.push(autor);
		$scope.livro.autor = '';
		console.log($scope.livro.autores);
	}
	
	$scope.removerAutor = function(indice){
		$scope.livro.autores.splice(indice,1);
	}
	
	$scope.getImage = function(){
		var bytes = $('#bytes').attr('src');
		var res = bytes.substring(22);
		$scope.livro.foto = [];
		$scope.livro.foto.push(res);
	}
	
	if($routeParams.id){
		
		BSRequest.get({path: "livros", id: $routeParams.id}, function(dado){
			$scope.livro = dado;
			$scope.livro.edicao = parseInt(dado.edicao);
			$scope.livro.preco = parseInt(dado.preco);
			$scope.livro.ano = parseInt(dado.ano);
			var foto = "data:image/png;base64,"+$scope.livro.foto;
			$scope.livro.foto = foto;
			$rootScope.categorias.forEach(function(categoria){
				if(categoria.id == $scope.livro.categoria.id){
					$scope.livro.categoria = categoria;
				}
			})
			$("#bytes").attr("src",$scope.livro.foto);
		}, function(erro){
			console.log(erro);
		});
		
		
		
		$scope.editar = true; 
		
	}
	
	$scope.removeImage = function(){
		$("#edit-remove").remove();
	}
	
	
	$scope.submeter = function(){
		$scope.livro.usuario = JSON.parse(window.localStorage.getItem("user"));
		$scope.livro.foto = '';
		var livro = angular.toJson($scope.livro);
		if($scope.livro.id){
			BSRequest.update({path: 'livros'}, livro, function(dados){
				console.log(dados);
				$rootScope.messages.successMessage="Livro "+$scope.livro.titulo+" atualizado com sucesso";
				$scope.livro = {};
				$scope.livro.autores = [];
				submeterFoto();
			}, function(erro){
				console.log(erro);
			});
		}else{
			BSRequest.save({path: 'livros'}, livro, function(dados){
				console.log(dados);
				$rootScope.messages.successMessage="Livro "+$scope.livro.titulo+" cadastrado com sucesso";
				$scope.livro = {};
				$scope.livro.autores = [];
				submeterFoto();
			}, function(erro){
				console.log(erro);
			});
		}
		
	}
	
	function submeterFoto(){
		$scope.getImage();
		var livro = angular.toJson($scope.livro);
		$http.post('http://localhost:8080/book-sales/rest/livros/foto',livro)
		.then(function(dado,status){
			console.log(status);
			$('#bytes').attr({src:''});
			$scope.livro = {};
		},function(erro){
			console.log(erro);
		});
	}
	
});