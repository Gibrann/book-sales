(function(){
	facController.controller('LivroController', function($scope, $routeParams, BSRequest,$rootScope){
		
		if(window.localStorage.getItem('user')){
			$rootScope.isLogado = true;
			var user = JSON.parse(window.localStorage.getItem('user'));
			$rootScope.userEmail = user.email;
		}
		
		var livroObjct = this;
		livroObjct.path = "livros";
		$scope.busca = "";
		$scope.livrosBuscados = [];
		$scope.comparator = false;
		$scope.filtro = "";
		$scope.livro = {};
		$scope.livros = [];
		
		if($routeParams.categoria){
			var categoria =  $routeParams.categoria
			$scope.comparator = true;
			
			$scope.categorias.forEach(cat => {
				
				if(cat.nome.toLowerCase() == categoria){
					$scope.filtro = categoria;
					return;
				}
				
			});
			
			if($scope.filtro.length == 0){
				$scope.filtro = "outros";
			}
		}
		
		BSRequest.get({path:livroObjct.path}, function(dados){
			if(Array.isArray(dados.livroVO)){
				$scope.livros = dados.livroVO;
			}else{
				$scope.livros.push(dados.livroVO);
			}
			
		}, function(erro){
			console.log(erro);
		});
		
		if($routeParams.livroId){
			
			BSRequest.get({path: livroObjct.path, id: $routeParams.livroId}, function(dado){
				$scope.livro = dado;
				$rootScope.currentBook = $scope.livro;
			}, function(erro){
				console.log(erro);
			});
			
		}	
		
		
		
	});
	
})();
