facController.controller('PesquisaController', function($scope,BSRequest,$routeParams,$location){
	
	$scope.filtroPesquisa = "";
	$scope.livrosPesquisa = [];
	$scope.message = "";
	var userObj = this;
	userObj.path = "livros";
	if($routeParams.filtro){
		$scope.filtroPesquisa = $routeParams.filtro;
		BSRequest.get({path: userObj.path, subPath: "busca", id: $scope.filtroPesquisa}, function(dados){
			if(typeof dados.livroVO != "undefined"){
				if(Array.isArray(dados.livroVO)){
					$scope.livrosPesquisa = dados.livroVO;
				}else{
					$scope.livrosPesquisa.push(dados.livroVO);
				}
			}else{
				$scope.message = "Não encontramos nenhum resultado correspondente a '"+$scope.filtroPesquisa+"'";
			}
		},function(erro){
			console.log(erro);
		});
	}else{
	  $location.url('/livros');
	}
	$scope.pesquisar = function(){
		$location.url('/pesquisa/'+$scope.filtroPesquisa);
	}
	
});