facController.controller('PerfilController', function($scope,saveOrUpdateObj,$rootScope,$location){
	
	if(!$rootScope.isLogado || $rootScope.isLogado == undefined){
		$location.url("login");
		$rootScope.messages.warnMessage ="Você precisa estar logado para acessar esta página! =(";
		return;
	}
	
	$rootScope.messages = {};
	$rootScope.messages.erroMessage = "";
	$rootScope.messages.successMessage = "";
	var user = window.localStorage.getItem('user');
	$scope.isEditing = false;
	$scope.usuario = JSON.parse(user);
	var path = "usuario";
	$scope.editar = function(){
		$scope.isEditing = true;
		$scope.$broadcast('editar');
	}
	
	$scope.atualizar = function(){
		if(verificarSenha()){
			saveOrUpdateObj.cadastrar($scope.usuario,path)
			.then(function(data){
				$rootScope.messages.successMessage = data.mensagem;
				$scope.usuario.senha = "";
				$scope.confSenha = "";
				$scope.$broadcast('editar');
				$scope.isEditing = false;
				$rootScope.userEmail = $scope.usuario.email;
				window.localStorage.setItem('user',angular.toJson($scope.usuario))
			},function(erro){
				$scope.usuario.senha = "";
				$scope.confSenha = "";
				$rootScope.messages.erroMessage = erro.mensagem;
			});
		}
	}
	
	function verificarSenha(){
		$rootScope.messages.erroMessage = "";
		if($scope.usuario.senha == $scope.confSenha){
			return true;
		}else{
			$rootScope.messages.erroMessage = "As senhas não conferem";
			return false;
		}
	}
	
});