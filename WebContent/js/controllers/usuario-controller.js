facController.controller('UsuarioController', function($scope,BSRequest,$cookies,$location, $rootScope){

	var userObj = this;
	userObj.path = "usuario";
	$scope.usuario = {};
	$scope.aux = {};
	$rootScope.userEmail = '';

	$scope.logar = function(){
		var usuario = angular.toJson($scope.usuario);
		console.log(usuario);
		BSRequest.save({path: userObj.path}, usuario, 
			function(dados){
				if(dados.email == undefined){
					$rootScope.messages.erroMessage = "Usuário não encontrado!"
				}else{
					var user = {"id": dados.id, "email": dados.email, "nome" : dados.nome}
					window.localStorage.setItem("user",angular.toJson(user));
					$scope.usuario = {};
					$rootScope.userEmail = dados.email;
					$location.url("/livros");
					$rootScope.isLogado = true;
				}
			}, function(erro){
				console.log(erro);
				$rootScope.messages.erroMessage = erro;
			});
	}
	
	function isPasswordsEqls(){
		if($scope.usuario.senha == $scope.aux.confirmarSenha){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.addNewUser = function(){
		if(isPasswordsEqls()){
			var usuario = angular.toJson($scope.usuario);
			BSRequest.save({path: userObj.path, subPath: "adicionar"}, usuario, function(dados){
				$rootScope.messages.successMessage = "Usuario "+$scope.usuario.nome+" cadastrado com sucesso!";
				$scope.usuario = {};
				$scope.aux = {};
			}, function(erro){
				console.log(erro);
			});
		}else{
			$rootScope.messages.erroMessage = "As senhas não conferem";
		}
	}
	
	$scope.logout = function(){
		window.localStorage.removeItem("user");
		$rootScope.isLogado = false;
		$rootScope.userEmail = '';
		$location.url("/livros");
	}

});