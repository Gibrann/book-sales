facController.controller('MeusLivrosController', function($scope,BSRequest,$rootScope,$location){
	
	if(!$rootScope.isLogado || $rootScope.isLogado == undefined){
		$location.url("login");
		$rootScope.messages.warnMessage ="Você precisa estar logado para acessar esta página! =(";
		return;
	}
	
	
	$scope.livros = [];
	$rootScope.messages = {};
	$rootScope.messages.erroMessage = "";
	$rootScope.messages.successMessage = "";
	var path = "livros";
	$scope.message = "Você não tem nenhum livro cadastrado no momento";
	function getLivros(){
		var user = JSON.parse(window.localStorage.getItem('user'));
		BSRequest.get({path: path, subPath: "user", id:user.id}, function(dados){
			$scope.livros = [];
			if(dados.livroVO){
				if(Array.isArray(dados.livroVO)){
					$scope.livros = dados.livroVO;
				}else{
					$scope.livros.push(dados.livroVO);
				}
			}
		}, function(erro){
			console.log(erro);
		});
	}
	
	getLivros();
	
	$scope.atualizar = function(id){
		$location.url("atualizar-livro/"+id);
	}
	
	
	$rootScope.remover = function(livro){
		BSRequest.remove({path: path, id:livro.id}, 
			function(data){
				getLivros();
				$rootScope.messages.successMessage= "O livro "+livro.titulo+" foi removido com sucesso!";
			},function(erro){
				$rootScope.messages.erroMessage = erro;
			});
	}
	
});