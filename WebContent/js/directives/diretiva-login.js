
var bk = angular.module('bookSales');

bk.directive('show', function(){
	
	var ddo = {};
	
	ddo.restrict = 'A';
	
	ddo.scope = {
		hide: '='	
	}
	
	return ddo;
	
});

bk.directive('noReadonly', function(){
	
	var ddo = {};
	
	ddo.restrict = "A";
	
	ddo.link = function(scope,element){
		scope.$on('editar', function(){
			if(element[0].getAttribute('readonly')){
				element[0].removeAttribute('readonly');
			}else{
				element[0].setAttribute('readonly','readonly');
			}
			
		});
	}
	
	return ddo;
	
});