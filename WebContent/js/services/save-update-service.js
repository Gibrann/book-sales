serviceModule.factory('saveOrUpdateObj', function(BSRequest, $q){

		var servico = {};

		servico.cadastrar = function(object, path){
			return $q(function(resolve, reject){
				if(object.id){
					var jsonObj = angular.toJson(object);
					BSRequest.update({path: path}, jsonObj, function(){
						resolve({
							mensagem: 'Transação realizada com sucesso!',
							isUpdate: true
						});
					}, function(erro){
						console.log(erro);
						reject({
							mensagem: 'Transação mal sucedida!'
						});
					})
				}else{
					BSRequest.save(object, function(){
						resolve({
							mensagem: 'Transação realizada com sucesso!',
							isUpdate: false
						});
					}, function(erro){
						reject({
							mensagem: 'Transação mal sucedida!'
						});
					});
				}
			});
		}
		
		
		return servico;

	});